package com.company;

public interface Politeness {
    public void sayGoodbye();
    void calculateAge();
    String addPrefix(String prefixName);

}
