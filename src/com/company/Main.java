package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Student Ivanov = new Student(17, "Jan", "Ivanovich");
     ///   Ivanov.goToScholl();
     ///  Ivanov.sayHello();

     //   Ivanov.setAge(17) ; при использ-и конструктора эти строки не нужны
     //   Ivanov.setName("Jan");
     //   Ivanov.setLastName("Ivanovich");
     //   System.out.println("Мне " + Ivanov.getAge() + " лет" + "    Моё имя и фамилия " + Ivanov.getName() + " " + Ivanov.getLastName());
        Ivanov.showInfo();

        Student vasya = new Student();

       // Teacher Ivanovich = new Teacher();
      //  Ivanovich.goToWork();
      //  Ivanovich.sayHello();
        System.out.println();
        Teacher Petrov = new Teacher(27, "Petr", "Petrovich");
        Petrov.showInfo();

        goodbye(Ivanov,"Sssstudent");
        goodbye(Petrov, "Ttteacher");
    }
    private static  void goodbye(Politeness human, String prefix){
        human.sayGoodbye();
        human.calculateAge();
        System.out.println(human.addPrefix(prefix));
    }
}
