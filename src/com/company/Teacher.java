package com.company;

import javax.sound.midi.Soundbank;

public class Teacher extends Person implements Politeness{

    public Teacher(int age, String name, String lastName){    //конструктор - это метод класса
        setAge(age);
        setName(name);
        setLastName(lastName);
    }

    public void goToWork(){
        System.out.println("Я работаю в школе!");
    }

    @Override
    public void sayGoodbye() {
        System.out.println("Goodbye from teacher"); }

    @Override
    public void calculateAge() {
        System.out.println(getAge()*10);
    }

    @Override
    public String addPrefix(String prefixName) {
        String someName = getName() + " "+ getLastName() + " "+ prefixName;
        return someName;
    }
}
