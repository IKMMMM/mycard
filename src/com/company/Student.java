package com.company;

public class Student extends Person implements Politeness {

    public Student(){}  // это полиморфизм   -  создали объект  - нового студента

    public Student(int age, String name, String lastName){    //конструктор - это метод класса
        setAge(age);
        setName(name);
        setLastName(lastName);
    }

    public void goToScholl(){
    System.out.println("Я иду в школу!");}

    @Override
    public void sayGoodbye() {
        System.out.println("Goodbye from student");
    }

    @Override
    public void calculateAge() {
        System.out.println(getAge() - 10);
    }

    @Override
    public String addPrefix(String prefixName) {
        String subname = prefixName +" "+ getName()+ " " + getLastName();
        return subname;
    }
}
